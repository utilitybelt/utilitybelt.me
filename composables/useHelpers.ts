import { LuaSourceMetaDTO } from "~/server/models/dto/LuaSourceMetaDTO"
import { createDefu } from 'defu'
import { twMerge } from 'tailwind-merge'

const safeChars = new RegExp(/[^a-z0-9-]/g)
const dashChars = new RegExp(/[\s_]/g);

const _lookup: {[text: string]: string} = {}

export function anchorId(text: string) {
  const cached = _lookup[text];
  if (cached) {
    return cached;
  }

  const anchorText = (text || '')
    .replace(/(\s+|[\(\)]|,)/g, '-')
    .replace(/([A-Z\-]+)/g, (g) => {
      if (g.length > 1) {
        if (g.endsWith('-')) {
          return g.toLowerCase()
        }
        else {
          return `${g.slice(0,-1).toLowerCase()}-${g.slice(-1).toLowerCase()}`
        }
      }
      return `-${g[0].toLowerCase()}`
    }) // camel to hyphenated
    .replace(dashChars, '-') // convert some chars to dashes
    .replace(safeChars, '') // only allow safe chars
    .replace(/-+/g, '-') // replace duplicated dashes with a single
    .replace(/^-+/, '') // remove starting dashes
    .replace(/-+$/, '') // removing trailing slashes
    .trim()
    .toLowerCase();
  
    _lookup[text] = anchorText;
  return anchorText;
}

export function paramToString(param: any) : string {
  let p = ''
  if (typeof(param) === 'string') {
    p = param as string
  }
  else if (typeof(param) === 'object' && param.length && param.length > 0) {
    p = param[0] as string
  }
  return p.toLowerCase()
}

export function toHexString(value: number) {
  return '0x' + value.toString(16).padStart(8, '0');
}

export function sourceMetaToLink(meta?: LuaSourceMetaDTO) {
  if (!meta) {
    return 'https://gitlab.com/utilitybelt/'
  }

  if (meta.repositoryType?.toLowerCase() == 'github') {
    const line = meta.startLine ? `#L${meta.startLine+1}${(meta.endLine && meta.endLine > meta.startLine ? `-L${meta.endLine+1}` : '')}` : ''
    return `https://github.com/${meta.repository}/blob/main/${meta.file}${line}`
  }
  else {
    const line = meta.startLine ? `#L${meta.startLine+1}${(meta.endLine && meta.endLine > meta.startLine ? `-${meta.endLine+1}` : '')}` : ''
    const baseUrl = `https://gitlab.com/${meta.repository?.includes('/') ? '' : 'utilitybelt/'}${meta.repository}/-/blob`
    return `${baseUrl}/${useAppConfig().git.branch}/${meta.file}${line}`
  }
}

export function padVersion(version?: string) {
  version = (version || '?.?.?').replace('v', '')
  return version.split('.').map(p => p.padStart(4, '0')).join()
}

export function omit<T extends Record<string, any>, K extends keyof T> (
  object: T,
  keysToOmit: K[] | any[]
): Pick<T, Exclude<keyof T, K>> {
  const result = { ...object }

  for (const key of keysToOmit) {
    delete result[key]
  }

  return result
}

export function get (object: Record<string, any>, path: (string | number)[] | string, defaultValue?: any): any {
  if (typeof path === 'string') {
    path = path.split('.').map(key => {
      const numKey = Number(key)
      return isNaN(numKey) ? key : numKey
    })
  }

  let result: any = object

  for (const key of path) {
    if (result === undefined || result === null) {
      return defaultValue
    }

    result = result[key]
  }

  return result !== undefined ? result : defaultValue
}

export const defuTwMerge = createDefu((obj, key, value) => {
  if (typeof obj[key] === 'string' && typeof value === 'string' && obj[key] && value) {
    // @ts-ignore
    obj[key] = twMerge(obj[key], value)
    return true
  }
})