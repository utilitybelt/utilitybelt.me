export default function useNavState() {
  return useState<Record<string, boolean>>('sidebar-nav-collapsed', () => ref({}))
}
