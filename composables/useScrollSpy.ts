import { theme } from '#tailwind-config'
import type { Ref } from 'vue'
import tailwindConfig from '~/tailwind.config'

export const useScrollspy = () => {
  const observer = ref() as Ref<IntersectionObserver>
  const visibleHeadings = ref([]) as Ref<string[]>
  const activeHeadings = ref([]) as Ref<string[]>

  const observerCallback = (entries: IntersectionObserverEntry[]) =>
    entries.forEach((entry) => {
      const id = entry.target.id

      // todo: this should account for the header, since we dont want elements behind the header to count as visible
      if (entry.isIntersecting) {
        visibleHeadings.value.push(id)
      } else {
        visibleHeadings.value = visibleHeadings.value.filter(t => t !== id)
      }
    })

  const updateHeadings = (headings: Element[]) =>
    headings.forEach((heading) => {
      observer.value.observe(heading)
    })

  watch(visibleHeadings, (val, oldVal) => {
    if (val.length === 0) { activeHeadings.value = oldVal } else { activeHeadings.value = val }
  }, { deep: true })

  onBeforeMount(() => (observer.value = new IntersectionObserver(observerCallback)))

  onBeforeUnmount(() => observer.value?.disconnect())

  return {
    visibleHeadings,
    activeHeadings,
    updateHeadings
  }
}