
export async function getModulePartials() {
  return await $fetch(`/api/lua/modules`)
}

export async function getLuaClass(moduleName: string, className: string) {
  return await $fetch(`/api/lua/modules/${moduleName.toLowerCase()}/classes/${className.toLowerCase()}`)
}

export async function getLuaAction(moduleName: string, className: string) {
  return await $fetch(`/api/lua/modules/${moduleName.toLowerCase()}/actions/${className.toLowerCase()}`)
}

export async function getLuaEventArgs(moduleName: string, className: string) {
  return await $fetch(`/api/lua/modules/${moduleName.toLowerCase()}/eventargs/${className.toLowerCase()}`)
}

export async function getLuaEnum(moduleName: string, enumName: string) {
  return await $fetch(`/api/lua/modules/${moduleName.toLowerCase()}/enums/${enumName.toLowerCase()}`)
}

export async function getLuaModule(moduleName: string) {
  return await $fetch(`/api/lua/modules/${moduleName.toLowerCase()}`)
}