import { useWebWorkerFn } from '@vueuse/core'
import { NavItemWithParent } from 'nav'
import { NavItem } from '@nuxt/content/dist/runtime/types';

export const { workerFn: filterNavItems } = useWebWorkerFn((initialLinks: NavItemWithParent[], filterText: string) => {
  function isNavFilterMatch(link: NavItemWithParent | NavItem, filter: string) {
    if (filter == '')
      return false
    if (link.title.toLowerCase().includes(filter)) {
      return true
    }
    for (const child of link.children || []) {
      if (isNavFilterMatch(child, filter)) {
        return true
      }
    }
    return false
  }
  
  const doFilter = (links: NavItemWithParent[], filter: string) : NavItemWithParent[] => {
    const results: NavItemWithParent[] = []
    for (const link of links) {
      if (isNavFilterMatch(link, filter)) {
        link.isExpanded = true
        results.push(link)
      }
    }
  
    return results
  }
  
  const filterLinks = (navItems: NavItemWithParent[] | undefined, filter: string) => {
    if (!navItems) return []
  
    return doFilter(navItems, filter)
  }

  return {
    links: filterLinks(initialLinks, filterText),
    filter: filterText
  }
})