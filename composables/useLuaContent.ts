import { LuaEnumDTO } from "~/server/models/dto/LuaEnumDTO"
import { LuaModuleDTO } from "~/server/models/dto/LuaModuleDTO"
import { LuaModulePartialDTO } from "~/server/models/dto/LuaModulePartialDTO"
import { LuaBaseTypeDTO } from "~/server/models/dto/LuaBaseTypeDTO"
import { LuaClassDTO } from "~/server/models/dto/LuaClassDTO"
import { LuaEventArgsDTO } from "~/server/models/dto/LuaEventArgsDTO"
import { NavItem, TocLink } from '@nuxt/content/dist/runtime/types'
import { getLuaAction, getLuaClass, getLuaEnum, getLuaEventArgs, getLuaModule, getModulePartials } from "./useLuaAPI"
import { LuaAPIType } from "~/server/models/LuaAPIType"
import { LuaConstructorDTO } from "~/server/models/dto/LuaConstructorDTO"
import { LuaMethodDTO } from "~/server/models/dto/LuaMethodDTO"
import { LuaEnumFieldDTO } from "~/server/models/dto/LuaEnumFieldDTO"
import { LuaFieldDTO } from "~/server/models/dto/LuaFieldDTO"
import { LuaEventDTO } from "~/server/models/dto/LuaEventDTO"

export const navSections = {
  'actions': 'Actions',
  'classes': 'Classes',
  'enums': 'Enums',
  'eventArgs': 'Event Arguments'
} as any

export const tocLinkSections = {
  'instances': 'Instances',
  'methods': 'Methods',
  'events': 'Events',
  'fields': 'Fields',
  'actions': 'Actions',
  'classes': 'Classes',
  'enums': 'Enums'
} as any

export const useLuaAPIState = () => {
  /**
   * Map of loaded modules.
   */
  const modules = useState<Record<string, LuaModuleDTO>>('luapi-modules', () => shallowRef(shallowReactive({})))

  /**
   * List of all partial modules (for nav / module listings)
   */
  const modulePartials = useState<LuaModulePartialDTO[]>('luapi-module-partials', () => ref([]))

  /**
   * Map of loaded enums.
   */
  const enums = useState<Record<string, LuaEnumDTO>>('luapi-enums', () => shallowRef(shallowReactive({})))

  /**
   * Map of loaded classes.
   */
  const classes = useState<Record<string, LuaClassDTO>>('luapi-classes', () => shallowRef(shallowReactive({})))

  /**
   * Map of loaded event args.
   */
  const eventArgs = useState<Record<string, LuaEventArgsDTO>>('luapi-eventargs', () => shallowRef(shallowReactive({})))

  /**
   * Map of generated tocs.
   */
  const tocs = useState<Record<string, TocLink[] | undefined>>('luapi-tocs', () => shallowRef(shallowReactive({})))

  /**
   * Map of all loaded LuaBaseTypeDTOs.
   */
  const types = useState<Record<string, LuaClassDTO | LuaEventArgsDTO | LuaEnumDTO | LuaConstructorDTO | LuaMethodDTO | LuaEnumFieldDTO | LuaFieldDTO | LuaEventDTO | undefined>>('luapi-basetypes', () => shallowRef(shallowReactive({})))

  return {
    modules,
    enums,
    classes,
    eventArgs,
    modulePartials,
    tocs,
    types
  }
}

export async function getLuaModulePartials() {
  const { modulePartials: modulePartialsState } = useLuaAPIState()

  if (!modulePartialsState.value || modulePartialsState.value.length == 0) {
    modulePartialsState.value = await getModulePartials()
  }

  return computed(() => modulePartialsState.value)
}

export async function getLuaEnumPageData(moduleName: string, enumName: string) {
  const { enums } = useLuaAPIState()

  const key = `${moduleName}:${enumName}`

  if (!enums.value[key]) {
    enums.value[key] = await getLuaEnum(moduleName, enumName)
  }

  const enumType = computed(() => enums.value[key])

  const toc = computed(() => {
    if (!enumType.value)
      return
    return [{
      text: 'Fields',
      id: 'fields',
      children: enumType.value.fields.map(f => {
        return { text: f.name, id: anchorId(f.name), depth: 1 }
      })
    }] as TocLink[]
  })

  return {
    enumType,
    toc
  }
}

export async function getLuaModulePageData(moduleName: string) {
  const { modules } = useLuaAPIState()

  if (!modules.value[moduleName]) {
    modules.value[moduleName] = await getLuaModule(moduleName)
  }

  const moduleType = computed(() => modules.value[moduleName])
  const isBaseModule = computed(() => moduleType.value && moduleType.value.id == 'base')

  const toc = computed(() => {
    if (!moduleType.value)
      return
  
    const globals = moduleType.value.globals as any;
    const results = Object.keys(tocLinkSections).map(globalKey => {
      return Object.keys(globals[globalKey] || {}).length > 0 ? {
        text: tocLinkSections[globalKey],
        id: anchorId(globalKey),
        children: Object.keys(globals[globalKey] || {}).map(k => {
          const child = globals[globalKey][k]
          return { text: k, id: anchorId(k) } as TocLink
        })
      } as TocLink : undefined
    })
  
    return results.filter(t => !!t) as TocLink[]
  })

  return {
    moduleType,
    toc,
    isBaseModule
  }
}

export async function getLuaClassPageData(moduleName: string, className: string) {
  const { classes } = useLuaAPIState()

  const key = `${moduleName}:${className}`

  if (!classes.value[key]) {
    classes.value[key] = await getLuaClass(moduleName, className)
  }

  const classType = computed(() => classes.value[key])

  const toc = computed(() => {
    if (!classType.value)
      return
    const toc: TocLink[] = []

    makeClassTocCategory('Fields', classType.value.fields, toc)
    makeClassTocCategory('Events', classType.value.events, toc)
    makeClassTocCategory('Constructors', classType.value.constructors, toc)
    makeClassTocCategory('Methods', classType.value.methods, toc)

    return toc
  })

  return {
    classType,
    toc
  }
}

export async function getLuaActionPageData(moduleName: string, className: string) {
  const { classes } = useLuaAPIState()

  const key = `${moduleName}:${className}`

  if (!classes.value[key]) {
    classes.value[key] = await getLuaAction(moduleName, className)
  }

  const classType = computed(() => classes.value[key])

  const toc = computed(() => {
    if (!classType.value)
      return
    const toc: TocLink[] = []

    makeClassTocCategory('Fields', classType.value.fields, toc)
    makeClassTocCategory('Events', classType.value.events, toc)
    makeClassTocCategory('Constructors', classType.value.constructors, toc)
    makeClassTocCategory('Methods', classType.value.methods, toc)

    return toc
  })

  return {
    classType,
    toc
  }
}

export async function getLuaEventArgsPageData(moduleName: string, className: string) {
  const { eventArgs } = useLuaAPIState()

  const key = `${moduleName}:${className}`

  if (!eventArgs.value[key]) {
    eventArgs.value[key] = await getLuaEventArgs(moduleName, className)
  }

  const classType = computed(() => eventArgs.value[key])

  const toc = computed(() => {
    if (!classType.value)
      return
    const toc: TocLink[] = []

    makeClassTocCategory('Fields', classType.value.fields, toc)
    makeClassTocCategory('Methods', classType.value.methods, toc)

    return toc
  })

  return {
    classType,
    toc
  }
}

export async function getLuaSideNav() : Promise<ComputedRef<NavItem>> {
  const modulePartials = await getLuaModulePartials()
  
  return computed(() => {
    const hasModules = modulePartials.value && modulePartials.value.length > 0

    if (!modulePartials.value) {
      return {title: 'Lua API',
        _path: '/lua',
        children: []
      }
    }

    return {
      title: 'Lua API',
      _path: '/lua',
      children: [
        { title: 'Overview', _path: '/lua' },
        { title: 'Lua Globals', _path: '/lua/globals' },
        {
          title: 'UB Globals',
          _path: '/lua/modules/base',
          children: hasModules ? makeModuleChildrenNav(modulePartials.value.find(m => m.id == 'base')!).children : undefined
        },
        {
          title: 'Modules',
          _path: '/lua/modules',
          children: hasModules ? modulePartials.value.filter(m => m.id != 'base').map(makeModuleChildrenNav) : undefined
        }
      ]
    }
  })
}

function makeClassTocCategory(name: string, fields: LuaBaseTypeDTO[] | undefined, toc: TocLink[]) {
  if (fields && fields.length > 0) {
    toc.push({
      text: name,
      id: anchorId(name),
      children: fields.map(f => {
        return { text: f.name, id: anchorId(f.name), depth: 1 }
      })
    } as TocLink)
  }
}

function makeModuleChildrenNav(module: LuaModulePartialDTO) {
  const children = Object.keys(navSections).map(navSectionKey => {
    const sectionChildren = ((module as any)[navSectionKey] || []) as string[]
    return sectionChildren.length == 0 ? false : {
      title: navSections[navSectionKey],
      _path: `/lua/modules/${module.id}/${navSectionKey}`,
      children: sectionChildren.map(child => {
        return {
          title: child,
          _path: `/lua/modules/${module.id}/${navSectionKey}/${child.toLowerCase()}`
        } as NavItem
      })
    } as NavItem
  }).filter(c => !!c) as NavItem[]

  return {
    title: module.name,
    _path: `/lua/modules/${module.id}`,
    children: [{ title: 'Overview', _path: `/lua/modules/${module.id}` }].concat(children)
  } as NavItem
}