import { LuaAPIType } from "~/server/models/LuaAPIType"

const _builtinTypes = ['string', 'number', 'boolean', 'table', 'nil', 'function', 'event']

export function isBuiltinType(type: string) {
  return _builtinTypes.includes(type)
}

export function isSystemType(type: string) {
  return type.startsWith('T:System.')
}

const badgeColors: Record<string, string> = {
  [LuaAPIType.Module]: 'ring-lime-600 dark:ring-lime-600 text-lime-600 dark:text-lime-600 bg-tran',
  [LuaAPIType.Class]: 'ring-purple-500 dark:ring-purple-400 text-purple-500 dark:text-purple-400 bg-tran',
  [LuaAPIType.Method]: 'ring-purple-400 dark:ring-purple-300 text-purple-400 dark:text-purple-300 bg-tran',
  [LuaAPIType.Function]: 'ring-purple-400 dark:ring-purple-300 text-purple-400 dark:text-purple-300 bg-tran',
  [LuaAPIType.Field]: 'ring-violet-500 dark:ring-violet-400 text-violet-500 dark:text-violet-400 bg-tran',
  [LuaAPIType.Event]: 'ring-yellow-600 dark:ring-yellow-600 text-yellow-600 dark:text-yellow-600',
  [LuaAPIType.Enum]: 'ring-indigo-700 dark:ring-indigo-400 text-indigo-700 dark:text-indigo-400',
  [LuaAPIType.EnumField]: 'ring-indigo-600 dark:ring-indigo-500 text-indigo-600 dark:text-indigo-500',
  [LuaAPIType.Generic]: 'red',
  [LuaAPIType.BuiltIn]: 'ring-sky-700 dark:ring-sky-700 text-sky-700 dark:text-sky-500 bg-tran cursor-auto',
  [LuaAPIType.Action]: 'ring-pink-600 dark:ring-pink-500 text-pink-600 dark:text-pink-500 bg-tran',
  [LuaAPIType.EventArgs]: 'ring-yellow-700 dark:ring-yellow-700 text-yellow-700 dark:text-yellow-700',
}

export const getBadgeColor = (type: LuaAPIType | string | undefined) => {
  const colorType = type || LuaAPIType.BuiltIn
  let classes = badgeColors[colorType]
  classes += ' badge bg-tran'
  if (colorType != LuaAPIType.BuiltIn) {
    classes += ' dark:hover:bg-gray-950 focus-visible:ring-2 focus-visible:ring-primary-500 dark:focus-visible:ring-primary-400'
  }
  return classes;
}