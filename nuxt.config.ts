// https://nuxt.com/docs/api/configuration/nuxt-config
import path from 'path'
import { readFileSync } from 'fs'
import { resolve } from 'path'
import { ScriptTypes } from './server/models/ScriptTypes';

const preRenderRoutes: string[] = [
  '/',
  '/releases',
  '/plugin',
  '/scripting',
  '/lua'
];

const typeDefs = JSON.parse(readFileSync(path.resolve("./data/scripttypes.json"), 'utf8')) as ScriptTypes

Object.values(typeDefs.modules).forEach(module => {
  preRenderRoutes.push(`/lua/modules/${module.id.toLowerCase()}`);
  module.actions.forEach(a => preRenderRoutes.push(`/lua/modules/${module.id.toLowerCase()}/actions/${a.name.toLowerCase()}`))
  module.classes.forEach(a => preRenderRoutes.push(`/lua/modules/${module.id.toLowerCase()}/classes/${a.name.toLowerCase()}`))
  module.eventArgs.forEach(a => preRenderRoutes.push(`/lua/modules/${module.id.toLowerCase()}/eventargs/${a.name.toLowerCase()}`))
  module.enums.forEach(a => preRenderRoutes.push(`/lua/modules/${module.id.toLowerCase()}/enums/${a.name.toLowerCase()}`))
})

export default defineNuxtConfig({
  ssr: true,
  nitro: {
    storage: {
      apicache: {
        driver: 'fs',
        base: resolve('./.nuxt/cache/')
      }
    },
    prerender: {
      crawlLinks: true,
      routes: preRenderRoutes,
      failOnError: false
    }
  },
  modules: [
    '@nuxt/content',
    '@nuxt/ui',
    '@nuxtjs/mdc',
    '@vueuse/nuxt',
  ],
  tailwindcss: {
    exposeConfig: true,
  },
  webpack: {
    analyze: {
      analyzerMode: 'static'
    },
    extractCSS: false
  },
  content: {
    documentDriven: true,
    highlight: {
      theme: {
        dark: 'github-dark',
        default: 'github-light'
      },
      preload: ['json', 'lua']
    },
    navigation: {
      fields: ['_id']
    }
  },
  ui: {
    global: true,
    icons: ['material-symbols', 'simple-icons', 'ph', 'fluent']
  }
})
