import { LuaAPIType } from '../models/LuaAPIType'

export default defineNitroPlugin((nitroApp) => {

  // @ts-ignore
  nitroApp.hooks.hook('content:file:beforeParse', async (file) => {
    const luaTypeLookup = await useLuaData().getTypes()
    if (file._id.endsWith('.md') && file.body) {
      // @ts-ignore
      file.body = (file.body as string).replace(/\[([^\]]+)\]\(([a-z]+:\S+)\)/ig, (match, p1, p2, offset, string) => {
        const typeId = (p2.includes('.') ? p2 : p2.split(':')[1])
          .replace('xref:T:', 'T:')
          .replace('xref:', 'T:')
          .replace('UBScript.', 'UtilityBelt.Scripting.')
        const t = luaTypeLookup[typeId.toLowerCase()]
        if (t) {
          const props: string[] = []

          props.push(`type="${t.type}"`)
          props.push(`url="${t.url}"`)
          props.push(`id="${t.id}"`)
          switch(t.type) {
            case LuaAPIType.Module:
            case LuaAPIType.Action:
            case LuaAPIType.Class:
            case LuaAPIType.EventArgs:
            case LuaAPIType.Enum:
              props.push(`name="${t.name}"`)
              break
            case LuaAPIType.EnumField:
            case LuaAPIType.Event:
            case LuaAPIType.Method:
            case LuaAPIType.Field:
              const parentNameParts = t.id.split('(')[0].split('.')
              const parentName = parentNameParts[parentNameParts.length - 2]
              props.push(`name="${parentName}.${t.name}"`)
              break
          }

          //console.log(`replaced ${match} with :LuaTypeRefBadge{${props.join(' ')}}`)

          return `:LuaTypeRefBadge{${props.join(' ')}}`
        }
        else {
          console.warn('Bad lua type ref in', file._id, p2)
        }
        return match
      })
    }
  })
})