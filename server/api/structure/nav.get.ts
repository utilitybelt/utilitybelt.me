import { serverQueryContent } from '#content/server'
import { getContentQuery } from './../../../node_modules/@nuxt/content/dist/runtime/utils/query'
import { ParsedContentMeta, ParsedContent, NavItem } from './../../../node_modules/@nuxt/content/dist/runtime/types'
import { createNav } from './../../../node_modules/@nuxt/content/dist/runtime/server/navigation'
import { useLuaData } from '~/server/utils/useLuaData'


export default defineEventHandler(async (event) => {  const query = getContentQuery(event)
  const contents = await serverQueryContent(event, query)
    .where({
      /**
       * Partial contents are not included in the navigation
       * A partial content is a content that has `_` prefix in its path
       */
      _partial: false,
      /**
       * Exclude any pages which have opted out of navigation via frontmatter.
       */
      navigation: {
        $ne: false
      }
    })
    .find()
    const dirConfigs = await serverQueryContent(event)
    .where({ _path: /\/_dir$/i, _partial: true })
    .find()

  const configs = dirConfigs.reduce((configs, conf: ParsedContent) => {
    if (conf.title?.toLowerCase() === 'dir') {
      conf.title = undefined
    }
    const key = conf._path!.split('/').slice(0, -1).join('/') || '/'
    configs[key] = {
      ...conf,
      // Extract meta from body. (non MD files)
      ...conf.body
    }
    return configs
  }, {} as Record<string, ParsedContentMeta>)

  const nav = createNav(contents as ParsedContentMeta[], configs)

  nav.unshift({
    title: 'Releases',
    _path: '/releases'
  } as NavItem)

  nav.push({
    title: 'Lua API',
    _path: '/lua',
    children: [
      { title: 'Overview', _path: '/lua' },
      { title: 'Lua Globals', _path: '/lua/lua-globals' },
      { title: 'UB Globals', _path: '/lua/modules/base' },
      { title: 'Modules', _path: '/lua/modules' }
    ]
  })

  return nav
})