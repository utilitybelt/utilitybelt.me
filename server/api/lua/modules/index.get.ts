import { LuaModulePartialDTO } from "~/server/models/dto/LuaModulePartialDTO";

export default defineEventHandler(async (event) => {
  const moduleId = (getRouterParam(event, 'module') || '').toLowerCase()
  const modulePartials = await useLuaData().getModulePartials()

  return await descriptionsToMarkdownAST<LuaModulePartialDTO[]>(modulePartials)
});