import { LuaEnumDTO } from "~/server/models/dto/LuaEnumDTO";

export default defineEventHandler(async (event) => {
  const moduleId = (getRouterParam(event, 'module') || '').toLowerCase()
  const typeName = (getRouterParam(event, 'type') || '').toLowerCase()
  const type = await useLuaData().getEnum(moduleId, typeName)

  return await descriptionsToMarkdownAST<LuaEnumDTO>(type)
});