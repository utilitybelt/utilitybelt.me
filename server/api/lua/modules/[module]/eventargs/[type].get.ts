import { LuaEventArgsDTO } from "~/server/models/dto/LuaEventArgsDTO";

export default defineEventHandler(async (event) => {
  const moduleId = (getRouterParam(event, 'module') || '').toLowerCase()
  const typeName = (getRouterParam(event, 'type') || '').toLowerCase()
  const type = await useLuaData().getEventArgs(moduleId, typeName)

  return await descriptionsToMarkdownAST<LuaEventArgsDTO>(type)
});