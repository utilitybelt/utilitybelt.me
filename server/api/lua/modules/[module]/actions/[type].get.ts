import { LuaClassDTO } from "~/server/models/dto/LuaClassDTO";

export default defineEventHandler(async (event) => {
  const moduleId = (getRouterParam(event, 'module') || '').toLowerCase()
  const typeName = (getRouterParam(event, 'type') || '').toLowerCase()
  const type = await useLuaData().getAction(moduleId, typeName)

  return await descriptionsToMarkdownAST<LuaClassDTO>(type)
});