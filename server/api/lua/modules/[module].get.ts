import { LuaModuleDTO } from "~/server/models/dto/LuaModuleDTO";

export default defineEventHandler(async (event) => {
  const moduleId = (getRouterParam(event, 'module') || '').toLowerCase()
  const module = await useLuaData().getModule(moduleId)

  return await descriptionsToMarkdownAST<LuaModuleDTO>(module)
});