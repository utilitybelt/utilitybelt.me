import { LuaAPIType } from "~/server/models/LuaAPIType";
import { LuaBaseTypeDTO } from "~/server/models/dto/LuaBaseTypeDTO";
import { LuaChildTypeDTO } from "~/server/models/dto/LuaChildTypeDTO";
import { LuaClassDTO } from "~/server/models/dto/LuaClassDTO";
import { LuaConstructorDTO } from "~/server/models/dto/LuaConstructorDTO";
import { LuaEnumDTO } from "~/server/models/dto/LuaEnumDTO";
import { LuaEnumFieldDTO } from "~/server/models/dto/LuaEnumFieldDTO";
import { LuaEventArgsDTO } from "~/server/models/dto/LuaEventArgsDTO";
import { LuaEventDTO } from "~/server/models/dto/LuaEventDTO";
import { LuaFieldDTO } from "~/server/models/dto/LuaFieldDTO";
import { LuaMethodDTO } from "~/server/models/dto/LuaMethodDTO";
import { descriptionsToMarkdownAST, pickBaseDTO } from "~/server/utils/useLuaData";

export default defineEventHandler(async (event) => {
  const typeName = decodeURI(getRouterParam(event, 'type') || '').toLowerCase()
  const typeData = await useLuaData().getType(typeName)

  let res = {
    ...await descriptionsToMarkdownAST<LuaBaseTypeDTO>(typeData) as LuaClassDTO | LuaEventArgsDTO | LuaEnumDTO | LuaConstructorDTO | LuaMethodDTO | LuaEnumFieldDTO | LuaFieldDTO | LuaEventDTO
  }

  switch (res.type) {
    case LuaAPIType.EnumField:
    case LuaAPIType.Event:
    case LuaAPIType.Field:
    case LuaAPIType.Method:
      const child = (res as LuaChildTypeDTO)
      const parent = child.parentId ? await useLuaData().getType(child.parentId) : undefined
      if (parent) {
        child.parent = await descriptionsToMarkdownAST<LuaBaseTypeDTO>(pickBaseDTO(parent))
      }
      break
  }

  return res
}); 