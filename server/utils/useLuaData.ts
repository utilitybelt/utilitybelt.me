import { ScriptBaseType, ScriptClass, ScriptConstructor, ScriptEnum, ScriptEnumField, ScriptEvent, ScriptEventArgs, ScriptField, ScriptGlobalType, ScriptMethod, ScriptModule, ScriptPartialSource, ScriptSourceMeta, ScriptTypeRef, ScriptTypes } from './../models/ScriptTypes'
import { LuaModulePartialDTO } from '../models/dto/LuaModulePartialDTO'
import { LuaModuleDTO } from '../models/dto/LuaModuleDTO'
import { LuaBaseTypeDTO } from '../models/dto/LuaBaseTypeDTO'
import { LuaClassDTO } from '../models/dto/LuaClassDTO'
import { LuaGlobalTypeDTO } from '../models/dto/LuaGlobalTypeDTO'
import { LuaMethodDTO } from '../models/dto/LuaMethodDTO'
import { LuaMethodParameterDTO } from '../models/dto/LuaMethodParameterDTO'
import { LuaTypeRefDTO } from '../models/dto/LuaTypeRefDTO'
import { LuaConstructorDTO } from '../models/dto/LuaConstructorDTO'
import { LuaMethodReturnDTO } from '../models/dto/LuaMethodReturnDTO'
import { LuaEventDTO } from '../models/dto/LuaEventDTO'
import { LuaFieldDTO } from '../models/dto/LuaFieldDTO'
import { LuaSourceMetaDTO } from '../models/dto/LuaSourceMetaDTO'
import { LuaEventArgsDTO } from '../models/dto/LuaEventArgsDTO'
import { LuaEnumDTO } from '../models/dto/LuaEnumDTO'
import { LuaEnumFieldDTO } from '../models/dto/LuaEnumFieldDTO'
import { LuaAPIType } from '../models/LuaAPIType'
import { resolve } from 'path'
import { readFileSync } from 'fs'
import { hash } from 'ohash'
import { createStorage, type Storage, Driver, builtinDrivers } from 'unstorage'
import { anchorId } from '~/composables/useHelpers'
import { LuaChildTypeDTO } from '../models/dto/LuaChildTypeDTO'

type ModuleDef = {
  partialDto: LuaModulePartialDTO
  dto: LuaModuleDTO
  classes: Record<string, LuaClassDTO>
  actions: Record<string, LuaClassDTO>
  eventArgs: Record<string, LuaEventArgsDTO>
  enums: Record<string, LuaEnumDTO>
}

type TypeCacheData = {
  types: Record<string, LuaBaseTypeDTO>,
  modules: Record<string, ModuleDef>
}

let _typeDefs: ScriptTypes
let _typeLookup: Record<string, LuaBaseTypeDTO> = {}
let _modules: Record<string, ModuleDef> = {}

let _modulesPromise: Promise<boolean>

export async function descriptionsToMarkdownAST<T>(obj: any) : Promise<T> {
  if (typeof(obj) === 'object') {
    for (const k of Object.keys(obj)) {
      const v = obj[k]
      if ((k == 'summary' || k == 'description') && typeof(v) === 'string') {
        const md = await parseDocsMarkDown(obj[k])
        obj[k] = k == 'summary' ? md?.children[0] : md
      }
      else if (typeof(v) == 'object') {
        obj[k] = await descriptionsToMarkdownAST(v)
      }
    }
  }
  return obj
}

export function pickBaseDTO(parent: LuaBaseTypeDTO) {
  return {
    id: parent.id,
    type: parent.type,
    name: parent.name,
    url: parent.url,
    module: parent.module,
    summary: parent.summary,
    version: parent.version,
    source: parent.source,
  }
}

export function useLuaData() {
  if (!_typeDefs) {
    _typeDefs = JSON.parse(readFileSync(resolve("./data/scripttypes.json"), 'utf8')) as ScriptTypes
  }

  return {
    getTypeDefs: function() : ScriptTypes {
      return _typeDefs
    },
    
    getModulePartials: async function() : Promise<LuaModulePartialDTO[]> {
      await this.tryBuildModules()
      return Object.values(_modules).map(m => m.partialDto)
    },

    getModuleDef: async function(id: string) : Promise<ModuleDef> {
      await this.tryBuildModules()
      const module = _modules[id.toLowerCase()]
      if (!module) {
        throw createError({
          statusCode: 404,
          statusMessage: `Could not find Module: "${id}"`,
        })
      }
      return module
    },

    getModule: async function(id: string) : Promise<LuaModuleDTO> {
      return (await this.getModuleDef(id.toLowerCase())).dto
    },

    getClass: async function(moduleId: string, name: string) : Promise<LuaClassDTO> {
      const module = await this.getModuleDef(moduleId)

      const cls = module.classes[name.toLowerCase()]
      if (!cls) {
        throw createError({
          statusCode: 404,
          statusMessage: `Could not find Class "${name}" in module "${moduleId}": ${Object.keys(module.classes).join(', ')}`,
        })
      }

      return cls
    },

    getAction: async function(moduleId: string, name: string) : Promise<LuaClassDTO> {
      const module = await this.getModuleDef(moduleId)

      const cls = module.actions[name.toLowerCase()]
      if (!cls) {
        throw createError({
          statusCode: 404,
          statusMessage: `Could not find Action "${name}" in module "${moduleId}": ${Object.keys(module.actions).join(', ')}`,
        })
      }

      cls.type = LuaAPIType.Action

      return cls
    },

    getEventArgs: async function(moduleId: string, name: string) : Promise<LuaEventArgsDTO> {
      const module = await this.getModuleDef(moduleId)

      const evtArgs = module.eventArgs[name.toLowerCase()]
      if (!evtArgs) {
        throw createError({
          statusCode: 404,
          statusMessage: `Could not find EventArgs "${name}" in module "${moduleId}"`,
        })
      }

      return evtArgs
    },

    getEnum: async function(moduleId: string, name: string) : Promise<LuaEnumDTO> {
      const module = await this.getModuleDef(moduleId)

      const enm = module.enums[name.toLowerCase()]
      if (!enm) {
        throw createError({
          statusCode: 404,
          statusMessage: `Could not find Enum "${name}" in module "${moduleId}"`,
        })
      }

      return enm
    },

    getType: async function(id: string) : Promise<LuaBaseTypeDTO> {
      await this.tryBuildModules()
      const type = _typeLookup[id.toLowerCase()]
      if (!type) {
        throw createError({
          statusCode: 404,
          statusMessage: `Could not find Type "${id}"`, 
        })
      }
      return type
    },

    getTypes: async function() : Promise<Record<string, LuaBaseTypeDTO>> {
      await this.tryBuildModules()
      return _typeLookup
    },

    tryBuildModules: async function() {
      //console.time('tryBuildModules');
      if (!_modulesPromise) {
        _modulesPromise = new Promise(async (resolve) => {
          const res = await getCacheModuleData()
          _modules = res.modules
          _typeLookup = res.types
          resolve(true)
        })
      }
      //console.timeEnd('tryBuildModules'); 

      return _modulesPromise
    },
  }
}

const apiStorage = () => useStorage('apicache')

const getCacheModuleData = async () => {
  const startTime = performance.now()
  let cached = await apiStorage().getItem<TypeCacheData>('luadata:typecache')

  console.log('Building Lua api data... this might take a few seconds')

  if (!cached) {
    const res = {
      types: {},
      modules: {}
    } as TypeCacheData

    //console.time('Built lua api data');
    res.modules = await buildModules(_typeDefs)

    // populate type cache and base type urls
    for (const module of Object.values(res.modules)) {
      res.types[module.dto.id.toLowerCase()] = module.dto
      module.dto.url = `/lua/modules/${module.dto.id}`
      module.partialDto.url = module.dto.url

      for (const cls of Object.values(module.classes)) {
        res.types[cls.id.toLowerCase()] = cls
        cls.url = `${module.dto.url}/classes/${cls.name.toLowerCase()}`
        for (const type of cls.methods || []) {
          res.types[type.id.toLowerCase()] = type
          type.url = `${cls.url}#${anchorId(type.name)}`
          type.parentId = cls.id
        }
        for (const type of cls.constructors || []) {
          res.types[type.id.toLowerCase()] = type
          type.url = `${cls.url}#${anchorId(type.name)}`
          type.parentId = cls.id
        }
        for (const type of cls.fields || []) {
          res.types[type.id.toLowerCase()] = type
          type.url = `${cls.url}#${anchorId(type.name)}`
          type.parentId = cls.id
        }
        for (const type of cls.events || []) {
          res.types[type.id.toLowerCase()] = type
          type.url = `${cls.url}#${anchorId(type.name)}`
          type.parentId = cls.id
        }
      }
      for (const cls of Object.values(module.actions)) {
        res.types[cls.id.toLowerCase()] = cls
        cls.type = LuaAPIType.Action
        cls.url = `${module.dto.url}/actions/${cls.name.toLowerCase()}`
        for (const type of cls.methods || []) {
          res.types[type.id.toLowerCase()] = type
          type.url = `${cls.url}#${anchorId(type.name)}`
          type.parentId = cls.id
        }
        for (const type of cls.fields || []) {
          res.types[type.id.toLowerCase()] = type
          type.url = `${cls.url}#${anchorId(type.name)}`
          type.parentId = cls.id
        }
      }
      for (const cls of Object.values(module.eventArgs)) {
        res.types[cls.id.toLowerCase()] = cls
        cls.type = LuaAPIType.EventArgs
        cls.url = `${module.dto.url}/eventargs/${cls.name.toLowerCase()}`
        for (const type of cls.methods || []) {
          res.types[type.id.toLowerCase()] = type
          type.url = `${cls.url}#${anchorId(type.name)}`
          type.parentId = cls.id
        }
        for (const type of cls.fields || []) {
          res.types[type.id.toLowerCase()] = type
          type.url = `${cls.url}#${anchorId(type.name)}`
          type.parentId = cls.id
        }
      }
      for (const enm of Object.values(module.enums)) {
        res.types[enm.id.toLowerCase()] = enm
        enm.url = `${module.dto.url}/enums/${enm.name.toLowerCase()}`
        for (const type of enm.fields || []) {
          type.parentId = enm.id
          res.types[type.id.toLowerCase()] = {
            ...type,
            url: `${enm.url}#${anchorId(type.name)}`,
            type: LuaAPIType.EnumField,
            module: enm.module,
          }
        }
      }
    }

    const fillTypeRefUrls = (typeRef: LuaTypeRefDTO): LuaTypeRefDTO => {
      typeRef.url = res.types[typeRef.id.toLowerCase()]?.url

      if (typeRef.typeArgs) {
        typeRef.typeArgs.forEach(a => fillTypeRefUrls(a))
      }

      return typeRef
    }
    
    for (const module of Object.values(res.modules)) {
      const classes = Object.values(module.classes)
        .concat(Object.values(module.actions))
        .concat(Object.values(module.eventArgs))

      const globalTypes = Object.values(module.dto.globals.classes || {})
        .concat(Object.values(module.dto.globals.actions || {}))
        .concat(Object.values(module.dto.globals.enums || {}))
        .concat(Object.values(module.dto.globals.instances || {}))

      for (const type of globalTypes) {
        type.url = res.types[type.id.toLowerCase()]?.url
      }

      for (const cls of classes) {
        cls.baseTypes?.forEach(b => b.url = res.types[b.id.toLowerCase()]?.url)
        for (const evt of cls.events || []) {
          evt.parentName = cls.name
          fillTypeRefUrls(evt.typeRef)
        }
        for (const method of cls.methods || []) {
          method.parentName = cls.name
          method.returns?.forEach(r => fillTypeRefUrls(r.typeRef))
          method.parameters?.forEach(p => fillTypeRefUrls(p.typeRef))
        }
        for (const method of cls.constructors || []) {
          method.parentName = cls.name
          method.returns?.forEach(r => fillTypeRefUrls(r.typeRef))
          method.parameters?.forEach(p => fillTypeRefUrls(p.typeRef))
        }
        for (const field of cls.fields || []) {
          field.parentName = cls.name
          fillTypeRefUrls(field.typeRef)
        }
      }
      for (const enm of Object.values(module.enums)) {
        for (const field of enm.fields) {
          field.parentName = enm.name
        }
      }

      for (const evtArg of Object.values(module.eventArgs)) {
        evtArg.usedBy = evtArg.usedBy.map(u => {
          const type = res.types[u.id.toLowerCase()]
          return {
            id: type.id,
            name: type.name,
            type: type.type,
            module: type.module,
            parentName: type.parentName,
            url: type.url
          } as LuaBaseTypeDTO
        })
      }
    }

    //await apiStorage().setItem('luadata:typecache', res)
    cached = res as TypeCacheData
  }

  const buildTime = Math.round((performance.now() - startTime) * 100) / 100
  //console.timeEnd('Built lua api data')
  console.log(`Finished Building Lua api data: ${Object.keys(cached.modules).length} modules and ${Object.keys(cached.types).length} types in ${buildTime}ms`) 

  return cached
}

async function buildModules(typeDefs: ScriptTypes) : Promise<Record<string, ModuleDef>> {
  const modules: Record<string, ModuleDef> = {}
  for (const module of Object.values(typeDefs.modules)) {
    modules[module.id.toLowerCase()] = {
      dto: await toModuleDTO(module),
      partialDto: await buildModulePartial(module),
      classes: await buildModuleClasses(module),
      actions: await buildModuleActions(module),
      eventArgs: await buildModuleEventArgs(module),
      enums: await buildModuleEnums(module)
    }
  }
  return modules
}

async function toModuleDTO(module: ScriptModule) : Promise<LuaModuleDTO> {
  const moduleDTO = {
    ...(await toBaseDTO(module)),
    description: module.description,
    classes: [],
    actions: [],
    eventArgs: [],
    enums: [],
    globals: {}
  } as LuaModuleDTO

  for (const [key, val] of Object.entries(module.globals.classes)) {
    if (!moduleDTO.globals.classes) moduleDTO.globals.classes = {}
    moduleDTO.globals.classes[key] = await toGlobalTypeDTO(val)
  }
  for (const [key, val] of Object.entries(module.globals.actions)) {
    if (!moduleDTO.globals.actions) moduleDTO.globals.actions = {}
    moduleDTO.globals.actions[key] = await toGlobalTypeDTO(val)
    moduleDTO.globals.actions[key].type = LuaAPIType.Action
  }
  for (const [key, val] of Object.entries(module.globals.instances)) {
    if (!moduleDTO.globals.instances) moduleDTO.globals.instances = {}
    moduleDTO.globals.instances[key] = await toGlobalTypeDTO(val)
    moduleDTO.globals.instances[key].type = LuaAPIType.Class
  }
  for (const [key, val] of Object.entries(module.globals.enums)) {
    if (!moduleDTO.globals.enums) moduleDTO.globals.enums = {}
    moduleDTO.globals.enums[key] = await toGlobalTypeDTO(val)
  }
  for (const [key, val] of Object.entries(module.globals.methods)) {
    if (!moduleDTO.globals.methods) moduleDTO.globals.methods = {}
    moduleDTO.globals.methods[key] = await toMethodDTO(val)
  }

  for (const cls of module.classes) {
    moduleDTO.classes.push(await toBaseDTO(cls))
  }
  for (const cls of module.actions) {
    moduleDTO.actions.push(await toBaseDTO(cls))
  }
  for (const cls of module.eventArgs) {
    cls.type = LuaAPIType.EventArgs
    moduleDTO.eventArgs.push(await toBaseDTO(cls))
  }
  for (const enm of module.enums) {
    moduleDTO.enums.push(await toBaseDTO(enm))
  }

  return moduleDTO
}

async function toMethodDTO(methodType: ScriptMethod) : Promise<LuaMethodDTO> {
  const methodDTO = {
    ...(await toBaseDTO(methodType)),
    isStatic: methodType.isStatic ? true : undefined,
    isBlocking: methodType.isBlocking ? true : undefined,
    description: methodType.description,
    parameters: [],
    returns: []
  } as LuaMethodDTO

  for (const param of methodType.parameters || []) {
    methodDTO.parameters.push({
      name: param.name,
      typeRef: await toTypeRefDTO(param.type),
      description: param.description,
      defaultValue: param.defaultValue,
      hasDefault: param.hasDefault ? true : undefined
    } as LuaMethodParameterDTO)
  }

  for (const ret of methodType.returns || []) {
    methodDTO.returns.push({
      typeRef: await toTypeRefDTO(ret.type),
      description: ret.description,
    } as LuaMethodReturnDTO)
  }

  return methodDTO
}

async function toTypeRefDTO(typeRef: ScriptTypeRef) : Promise<LuaTypeRefDTO> {
  const typeRefDTO = {
    id: typeRef.id,
    name: typeRef.name,
    parent: typeRef.parent,
    type: typeRef.type,
    module: typeRef.module,
    isNullable: typeRef.isNullable ? true : undefined,
    isArray: typeRef.isArray ? true : undefined,
    genericType: typeRef.genericType
  } as LuaTypeRefDTO

  if (typeRef.args && typeRef.args.length > 0) {
    typeRefDTO.typeArgs = []
    for (const arg of typeRef.args) {
      typeRefDTO.typeArgs.push(await toTypeRefDTO(arg))
    }
  }
  if (typeRef.returns) {
    typeRefDTO.returns = await toTypeRefDTO(typeRef.returns)
  }

  return typeRefDTO;
}

async function toGlobalTypeDTO(globalType: ScriptGlobalType) : Promise<LuaGlobalTypeDTO> {
  return {
    id: globalType.id,
    summary: globalType.summary,
    name: globalType.name,
    version: globalType.version,
    type: globalType.type
  }
}

async function buildModulePartial(module: ScriptModule) : Promise<LuaModulePartialDTO> {
  return {
    ...(await toBaseDTO(module)),
    classes: module.classes.map(c => c.name),
    actions: module.actions.map(c => c.name),
    eventArgs: module.eventArgs.map(c => c.name),
    enums: module.enums.map(c => c.name)
  }
}

async function buildModuleEnums(module: ScriptModule) : Promise<Record<string, LuaEnumDTO>> {
  const enums: Record<string, LuaEnumDTO> = {}
  for (const enm of module.enums) {
    enums[enm.name.toLowerCase()] = await toEnumDTO(enm)
  }
  return enums
}

async function toEnumDTO(enm: ScriptEnum) : Promise<LuaEnumDTO> {
  const enmDTO = {
    ...(await toBaseDTO(enm)),
    isFlags: !!enm.isFlags,
    fields: [] as LuaEnumFieldDTO[]
  } as LuaEnumDTO

  for (const type of enm.fields) {
    enmDTO.fields.push(await toEnumFieldDTO(type))
  }

  return enmDTO
}
async function toEnumFieldDTO(field: ScriptEnumField) : Promise<LuaEnumFieldDTO> {
  const dto = {
    id: field.id,
    type: LuaAPIType.EnumField,
    module: '',
    value: field.value,
    name: field.name,
    version: field.version,
    description: field.description
  }

  return dto
}

async function buildModuleEventArgs(module: ScriptModule) : Promise<Record<string, LuaEventArgsDTO>> {
  const classes: Record<string, LuaEventArgsDTO> = {}
  for (const cls of module.eventArgs) {
    classes[cls.name.toLowerCase()] = await toEventArgsDTO(cls)
  }
  return classes
}

async function toEventArgsDTO(evtArgs: ScriptEventArgs) : Promise<LuaEventArgsDTO> {
  const evtArgsDTO = {
    ...(await toClassDTO(evtArgs)),
    usedBy: []
  } as LuaEventArgsDTO

  evtArgsDTO.constructors = []

  for (const type of evtArgs.usedBy) {
    evtArgsDTO.usedBy.push(await toBaseDTO(type))
  }

  return evtArgsDTO
}

async function buildModuleActions(module: ScriptModule) : Promise<Record<string, LuaClassDTO>> {
  const classes: Record<string, LuaClassDTO> = {}
  for (const cls of module.actions) {
    classes[cls.name.toLowerCase()] = await toClassDTO(cls)
  }
  return classes
}

async function buildModuleClasses(module: ScriptModule) : Promise<Record<string, LuaClassDTO>> {
  const classes: Record<string, LuaClassDTO> = {}
  for (const cls of module.classes) {
    classes[cls.name.toLowerCase()] = await toClassDTO(cls)
  }
  return classes
}

async function toClassDTO(cls: ScriptClass) : Promise<LuaClassDTO> {
  const classDTO = {
    ...(await toBaseDTO(cls)),
    description: cls.description,
    isStatic: cls.isStatic ? true : undefined,
  } as LuaClassDTO

  if (cls.baseTypes && cls.baseTypes.length > 0) {
    classDTO.baseTypes = []
    for (const baseType of cls.baseTypes) {
      classDTO.baseTypes.push(await toBaseDTO(baseType))
    }
  }

  if (cls.constructors && cls.constructors.length > 0) {
    classDTO.constructors = []
    for (const type of cls.constructors) {
      classDTO.constructors.push(await toConstructorDTO(type))
    }
  }

  if (cls.methods && cls.methods.length > 0) {
    classDTO.methods = []
    for (const type of cls.methods) {
      classDTO.methods.push(await toMethodDTO(type))
    }
  }

  if (cls.events && cls.events.length > 0) {
    classDTO.events = []
    for (const type of cls.events) {
      classDTO.events.push(await toEventDTO(type))
    }
  }

  if (cls.fields && cls.fields.length > 0) {
    classDTO.fields = []
    for (const type of cls.fields) {
      classDTO.fields.push(await toFieldDTO(type))
    }
  }

  if (cls.partials && cls.partials.length > 0) {
    classDTO.partials = []
    for (const type of cls.partials) {
      classDTO.partials.push(await toSourceMetaDTO(type))
    }
  }

  return classDTO
}

async function toSourceMetaDTO(sourceMeta: ScriptPartialSource) : Promise<LuaSourceMetaDTO> {
  return {
    file: sourceMeta.file,
    repository: sourceMeta.repository,
    repositoryType: sourceMeta.repositoryType, 
    startLine: sourceMeta.startLine,
    endLine: sourceMeta.endLine,
    version: sourceMeta.version,
  }
}

async function toFieldDTO(field: ScriptField) : Promise<LuaFieldDTO> {
  return {
    ...(await toBaseDTO(field)),
    isReadOnly: field.isReadOnly ? true : undefined,
    isStatic: field.isStatic ? true : undefined,
    typeRef: await toTypeRefDTO(field.typeRef),
    description: field.description
  }
}

async function toEventDTO(evt: ScriptEvent) : Promise<LuaEventDTO> {
  const typeRef = await toTypeRefDTO(evt.argsType)
  typeRef.type = LuaAPIType.Event
  return {
    ...(await toBaseDTO(evt)),
    typeRef: typeRef,
    description: evt.description
  }
}

async function toConstructorDTO(ctor: ScriptConstructor) : Promise<LuaConstructorDTO> {
  const methodDTO = {
    ...(await toBaseDTO(ctor)),
    ...(await toMethodDTO(ctor))
  } as LuaConstructorDTO

  methodDTO.id = methodDTO.id.replace(/#/g, '')

  return methodDTO
}

async function toBaseDTO(baseType: ScriptBaseType) {
  return {  
    id: baseType.id,
    type: baseType.type,
    name: baseType.name,
    module: baseType.module,
    summary: getSummary(baseType.description),
    version: baseType.version,
    source: baseType.source,
  }
}

function getSummary(description?: string) {
  return description ? description.split('\n')[0] : undefined
}