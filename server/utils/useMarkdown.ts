
//@ts-ignore
import { parseMarkdown } from '@nuxtjs/mdc/runtime'

export async function parseDocsMarkDown(mdc: string | undefined) {
  if (!mdc) {
    return
  }

  const file = {
    _id: 'lua-api-stuff.md',
    body: mdc
  }

  //@ts-ignore
  await useNitroApp().hooks.callHook('content:file:beforeParse', file)
 
  const ast = await parseMarkdown(file.body)

  return ast && ast.body
}