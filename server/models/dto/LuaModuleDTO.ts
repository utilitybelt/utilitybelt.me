import type { LuaBaseTypeDTO } from "./LuaBaseTypeDTO"
import type { LuaGlobalTypeDTO } from "./LuaGlobalTypeDTO"
import type { LuaMethodDTO } from "./LuaMethodDTO"

export type LuaModuleDTO = {
  description?: any
  classes: LuaBaseTypeDTO[]
  actions: LuaBaseTypeDTO[]
  eventArgs: LuaBaseTypeDTO[]
  enums: LuaBaseTypeDTO[]

  globals: {
    actions?: Record<string, LuaGlobalTypeDTO>
    classes?: Record<string, LuaGlobalTypeDTO>
    enums?: Record<string, LuaGlobalTypeDTO>
    instances?: Record<string, LuaGlobalTypeDTO>
    methods?: Record<string, LuaMethodDTO>
  }
} & LuaBaseTypeDTO