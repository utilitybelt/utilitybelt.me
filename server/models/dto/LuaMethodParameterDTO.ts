import type { LuaTypeRefDTO } from "./LuaTypeRefDTO"

export type LuaMethodParameterDTO = {
  name: string
  typeRef: LuaTypeRefDTO
  description?: any
  defaultValue?: any
  hasDefault?: boolean
}