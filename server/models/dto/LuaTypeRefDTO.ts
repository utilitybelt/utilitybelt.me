import type { LuaAPIType } from "../LuaAPIType"

export type LuaTypeRefDTO = {
  id: string
  name: string
  type: LuaAPIType
  url?: string
  module: string
  isNullable?: boolean
  isArray?: boolean
  genericType?: string
  typeArgs?: LuaTypeRefDTO[]
  returns?: LuaTypeRefDTO
}