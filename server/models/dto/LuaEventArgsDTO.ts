import type { LuaBaseTypeDTO } from "./LuaBaseTypeDTO";
import type { LuaClassDTO } from "./LuaClassDTO";

export type LuaEventArgsDTO = {
  usedBy: LuaBaseTypeDTO[]
} & LuaClassDTO