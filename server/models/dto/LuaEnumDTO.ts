import type { LuaBaseTypeDTO } from "./LuaBaseTypeDTO"
import type { LuaEnumFieldDTO } from "./LuaEnumFieldDTO"

export type LuaEnumDTO = {
  isFlags: boolean
  description?: any
  fields: LuaEnumFieldDTO[]
} & LuaBaseTypeDTO