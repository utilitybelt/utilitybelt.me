import type { LuaBaseTypeDTO } from "./LuaBaseTypeDTO"

export type LuaModulePartialDTO = {
  classes: string[]
  actions: string[]
  eventArgs: string[]
  enums: string[]

} & LuaBaseTypeDTO