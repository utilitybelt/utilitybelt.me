import type { LuaChildTypeDTO } from "./LuaChildTypeDTO"
import type { LuaTypeRefDTO } from "./LuaTypeRefDTO"

export type LuaEventDTO = {
  typeRef: LuaTypeRefDTO
  description?: any
} & LuaChildTypeDTO