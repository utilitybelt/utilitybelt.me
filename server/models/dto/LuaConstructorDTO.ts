import type { LuaMethodDTO } from "./LuaMethodDTO"

export type LuaConstructorDTO = {
} & LuaMethodDTO