import type { LuaBaseTypeDTO } from "./LuaBaseTypeDTO"
import type { LuaConstructorDTO } from "./LuaConstructorDTO"
import type { LuaEventDTO } from "./LuaEventDTO"
import type { LuaFieldDTO } from "./LuaFieldDTO"
import type { LuaMethodDTO } from "./LuaMethodDTO"
import type { LuaSourceMetaDTO } from "./LuaSourceMetaDTO"

export type LuaClassDTO = {
  isStatic?: boolean
  baseTypes?: LuaBaseTypeDTO[]
  fields?: LuaFieldDTO[]
  events?: LuaEventDTO[]
  constructors?: LuaConstructorDTO[]
  methods?: LuaMethodDTO[]
  description?: any
  partials?: LuaSourceMetaDTO[]
} & LuaBaseTypeDTO