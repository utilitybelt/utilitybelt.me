export type LuaSourceMetaDTO = {
  file?: string;
  repository?: string;
  repositoryType?: string;
  startLine?: number;
  endLine?: number;
  version?: string;
}