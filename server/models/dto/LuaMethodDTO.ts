import type { LuaChildTypeDTO } from "./LuaChildTypeDTO";
import type { LuaMethodParameterDTO } from "./LuaMethodParameterDTO";
import type { LuaMethodReturnDTO } from "./LuaMethodReturnDTO";

export type LuaMethodDTO = {
  isStatic?: boolean
  isBlocking?: boolean
  description?: any
  parameters: LuaMethodParameterDTO[]
  returns: LuaMethodReturnDTO[]
} & LuaChildTypeDTO