import type { LuaTypeRefDTO } from "./LuaTypeRefDTO"

export type LuaMethodReturnDTO = {
  typeRef: LuaTypeRefDTO
  description?: any
}