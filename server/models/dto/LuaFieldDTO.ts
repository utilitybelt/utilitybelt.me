import type { LuaChildTypeDTO } from "./LuaChildTypeDTO"
import type { LuaTypeRefDTO } from "./LuaTypeRefDTO"

export type LuaFieldDTO = {
  isReadOnly?: boolean
  isStatic?: boolean
  typeRef: LuaTypeRefDTO
  description?: any
} & LuaChildTypeDTO