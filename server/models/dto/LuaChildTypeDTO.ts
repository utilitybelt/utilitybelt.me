import type { LuaBaseTypeDTO } from "./LuaBaseTypeDTO";

export type LuaChildTypeDTO = {
  parentId?: string,
  parent?: LuaBaseTypeDTO
} & LuaBaseTypeDTO