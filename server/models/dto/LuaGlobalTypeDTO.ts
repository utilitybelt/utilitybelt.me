import type { LuaAPIType } from "../LuaAPIType"

export type LuaGlobalTypeDTO = {
  id: string
  name: string
  type: LuaAPIType
  parent?: string
  url?: string
  version?: string
  summary: string
}