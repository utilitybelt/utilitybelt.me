import type { LuaAPIType } from "../LuaAPIType"

export type LuaBaseTypeDTO = {
  id: string
  type: LuaAPIType
  name: string
  parentName?: string
  url?: string
  module: string
  summary?: any
  version?: string
  source?: string
}