import type { LuaChildTypeDTO } from "./LuaChildTypeDTO"

export type LuaEnumFieldDTO = {
  value: number
  description?: any
} & LuaChildTypeDTO