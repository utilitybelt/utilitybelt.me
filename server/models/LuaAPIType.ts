export enum LuaAPIType {
  Module = 'module',
  Class = 'class',
  Method = 'method',
  Field = 'field',
  Event = 'event',
  Enum = 'enum',
  EnumField = 'enumfield',
  Generic = 'generic',
  BuiltIn = 'builtin',
  Action = 'action',
  EventArgs = 'eventargs',
  Function = 'function'
}