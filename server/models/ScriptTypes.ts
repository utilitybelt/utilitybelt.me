import { LuaAPIType } from "./LuaAPIType";

export type ScriptTypes = {
  modules: Record<string, ScriptModule>
}

export type ScriptSourceMeta = {
  version?: string
  source?: string
}

export type ScriptTypeRef = {
  id: string
  name: string
  module: string
  parent?: string
  type: LuaAPIType
  genericType: string
  args: ScriptTypeRef[]
  returns: ScriptTypeRef
  isNullable: boolean
  isArray: boolean
  isBlocking: boolean
}

export type ScriptBaseType = {
  id: string
  name: string
  parent?: string
  module: string
  type: LuaAPIType
  description?: string
} & ScriptSourceMeta

export type ScriptModule = {
  classes: ScriptClass[]
  actions: ScriptClass[]
  eventArgs: ScriptEventArgs[]
  enums: ScriptEnum[]
  globals: ScriptModuleGlobals
} & ScriptBaseType

export type ScriptGlobalType = {
  id: string
  name: string
  type: LuaAPIType
  version?: string
  summary: string
}

export type ScriptModuleGlobals = {
  classes: Record<string, ScriptGlobalType>
  actions: Record<string, ScriptGlobalType>
  enums: Record<string, ScriptGlobalType>
  instances: Record<string, ScriptGlobalType>
  methods: Record<string, ScriptMethod>
}

export type ScriptEnum = {
  fields: ScriptEnumField[]
  isFlags?: boolean
} & ScriptBaseType

export type ScriptEnumField = {
  value: number
} & ScriptBaseType

export type ScriptPartialSource = {
  file?: string
  repository?: string
  repositoryType?: string
  startLine?: number
  endLine?: number
  version?: string
}

export type ScriptClass = {
  isStatic?: boolean
  constructors: ScriptConstructor[]
  methods: ScriptMethod[]
  fields: ScriptField[]
  events: ScriptEvent[]
  baseTypes: ScriptBaseType[]
  partials?: ScriptPartialSource[]
} & ScriptBaseType

export type ScriptEventArgs = {
  usedBy: ScriptBaseType[]
} & ScriptClass

export type ScriptConstructor = {
  parameters?: ScriptMethodParameter[]
} & ScriptBaseType

export type ScriptEvent = {
  argsType: ScriptTypeRef
} & ScriptBaseType

export type ScriptMethod = {
  isStatic?: boolean
  isBlocking?: boolean
  overrides?: string
  parameters?: ScriptMethodParameter[]
  returns?: ScriptMethodReturn[]
} & ScriptBaseType

export type ScriptMethodParameter = {
  name: string
  type: ScriptTypeRef
  description?: string
  defaultValue?: any
  hasDefault?: boolean
}

export type ScriptMethodReturn = {
  type: ScriptTypeRef
  description?: string
}

export type ScriptField = {
  isReadOnly?: boolean
  isStatic?: boolean
  typeRef: ScriptTypeRef
} & ScriptBaseType