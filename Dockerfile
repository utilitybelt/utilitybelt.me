FROM node:16-alpine AS builder

WORKDIR /usr/src/app/
COPY package.json ./
COPY yarn.lock ./

#RUN npm set registry http://verdaccio:4873
#RUN yarn config set yarn-offline-mirror /cache/yarn/offline
RUN time yarn --cache-folder /cache/yarn install

COPY . .

RUN time yarn build

FROM node:16-alpine AS final

RUN apk add --no-cache curl

WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/.output .
COPY content content
COPY data data

RUN touch /usr/src/app/HEALTHY

ARG BUILDVERSION
ENV BUILDVERSION=$BUILDVERSION

HEALTHCHECK  --interval=5s --timeout=2s --start-period=5s \
  CMD curl --fail "http://localhost:3000/" || exit 1

CMD [ "node", "./server/index.mjs" ] 