---
uid: Scripting.Specifics.APIInspector
---

# API Inspector

The API Inspector allows you to browse and inspect all the data exposed via the scripting api. You can view variables, call methods, subscribe to events, etc. To enable it, open UtilityBelt Service settings, go to ScriptSettings, and check EnableAPIInspector:

![API Inspector enable setting](/_nuxt/assets/images/docs/api-inspector-enable.png)

 