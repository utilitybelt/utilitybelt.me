const axios = require('axios');

module.exports = class Portainer {
  constructor(apiEndpoint, username, password) {
    this.apiEndpoint = apiEndpoint
    this.username = username
    this.password = password
    this.jwt = undefined
  }

  sleep(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }

  logAxiosError(error) {
    if (error.request) {
      console.error("Request body:", error?.request?.body)
      console.error("HTTP status: ", error?.response?.status)
      console.error("Response data:", error?.response?.data)
    }
    else {
      console.log(error)
    }
  }

  getAxiosConfig() {
    return {
      headers: {
        'Authorization': 'Bearer ' + this.jwt
      }
    }
  }

  async auth() {
    try {
      const authResponse = await axios.post(`${this.apiEndpoint}/auth`, {
        username: this.username,
        password: this.password
      })

      if (!authResponse.data || !authResponse.data.jwt) {
        return false;
      }

      this.jwt = authResponse.data.jwt

      return true;
    }
    catch (error) {
      this.logAxiosError(error)
      return false;
    }
  }

  async getStacks() {
    try {
      const existingStacksResponse = await axios.get(`${this.apiEndpoint}/stacks`, this.getAxiosConfig())

      return existingStacksResponse.data;
    }
    catch (error) {
      this.logAxiosError(error)
      return false;
    }
  }

  async createStack(name, stackFileContent) {
    try {
      const stackCreateResponse = await axios.post(`${this.apiEndpoint}/stacks?endpointId=2&method=string&type=2`, {
        Name: name,
        StackFileContent: stackFileContent
      }, this.getAxiosConfig())

      return stackCreateResponse.data
    }
    catch (error) {
      this.logAxiosError(error)
      return false;
    }
  }

  async removeOldStacks(startsWith, myStackName, stackId) {
    const stacks = await this.getStacks()
    let promises = []
    for (var i = 0; i < stacks.length; i++) {
      const stack = stacks[i];
      if (stack.Name.startsWith(startsWith) && stack.Name != myStackName) {
        let promise = (async () => {
          const containers = await this.getStackContainers(stack.Name)
          for (let i = 0; i < containers.length; i++) {
            const container = containers[i]
            const parts = containers[i].Image.split('/')
            const imageName = parts[parts.length - 1].split(':')[0]
            const imageTag = parts[parts.length - 1].split(':')[1]
            let serviceName = '';

            Object.keys(container.Labels).forEach((label) => {
              if (label.startsWith('traefik.http.routers.') && label.endsWith('.rule')) {
                serviceName = label.split('.')[3]
              }
            })

            console.log('Found old container:', container.Names[0])
            console.log('  - Image:', imageName)
            console.log('  - Tag:', imageTag)
            console.log('  - Service:', serviceName)

            const command = imageName.includes('api-server') ? 'rm /usr/src/app/HEALTHY' : 'rm /usr/share/nginx/html/health/index.html'
            await this.execContainer(container.Id, ['/bin/sh', '-c', command])
            console.log('  ... made it unhealthy')
          }
          await this.sleep(5000)
          await this.removeStack(stack.Name, stack.Id);
        })()
        promises.push(promise)
      }
    }

    await Promise.all(promises)
  }

  async removeStack(stackName, stackId) {
    try {
      console.log("Deleting stack:", stackName)
      //const stopResponse = await axios.post(`${apiEndpoint}/stacks/${stack.Id}/stop?endpointId=2`, {}, getAxiosConfig())
      //console.log("   ... Stopped stack:", stack.Name)
      await axios.delete(`${this.apiEndpoint}/stacks/${stackId}?endpointId=2`, this.getAxiosConfig())
      console.log("   ... Deleted stack:", stackName)
    }
    catch (error) {
      console.log("   ... Error deleting stack:", stackName)
      this.logAxiosError(error)
      return false
    }
  }

  async getStackContainers(stackName) {
    try {
      const filters = JSON.stringify({ "label": [`com.docker.compose.project=${stackName}`] });
      const containersResponse = await axios.get(`${this.apiEndpoint}/endpoints/2/docker/containers/json?all=1&filters=${filters}`, this.getAxiosConfig())
      return containersResponse.data
    }
    catch (error) {
      this.logAxiosError(error)
      return []
    }
  }

  async getUniqueStackName(stackNameBase) {
    let existingStacks = await this.getStacks()
    let stackName = stackNameBase
    let nextId = 1

    while (true) {
      let foundName = false

      existingStacks.forEach((stack) => {
        if (stack.Name == stackName) {
          foundName = true
        }
      })

      if (!foundName)
        break

      stackName = `${stackNameBase}-${nextId++}`
    }

    return stackName
  }

  async execContainer(containerId, command) {
    try {
      const execResponse = await axios.post(`${this.apiEndpoint}/endpoints/2/docker/containers/${containerId}/exec`, {
        "AttachStdin": false,
        "AttachStdout": true,
        "AttachStderr": true,
        "Tty": false,
        "Cmd": command,
        "Privileged": true
      }, this.getAxiosConfig())

      const execStartResponse = await axios.post(`${this.apiEndpoint}/endpoints/2/docker/exec/${execResponse.data.Id}/start`, {}, this.getAxiosConfig())

      console.log(execStartResponse.data)

      return true
    }
    catch (error) {
      this.logAxiosError(error)
      return false
    }
  }

  async waitForStackHealth(stackName) {
    // todo: dont infinite loop... destroy new stack if never healthy 
    // todo: we need to look at traefik instead of portainer, to make sure the router is active
    console.log('Waiting for healthy stack containers:');
    while (true) {
      if (await this.isStackHealthy(stackName)) {
        console.log("   ... Stack is healthy")
        break;
      }
      await this.sleep(2000)
    }
  }

  async isStackHealthy(stackName) {
    var filters = JSON.stringify({ "label": [`com.docker.compose.project=${stackName}`] });

    try {
      let isHealthy = true;
      const containersResponse = await axios.get(`${this.apiEndpoint}/endpoints/2/docker/containers/json?all=1&filters=${filters}`, this.getAxiosConfig())

      if (!containersResponse.data || !containersResponse.data.length)
        return false;

      containersResponse.data.forEach(r => {
        console.log(`   ... ${r.Names[0]} Status:`, r.Status)
        console.log(JSON.stringify(r, null, 2))
        if (!r.Status.includes('(healthy)'))
          isHealthy = false;
      })

      return isHealthy
    }
    catch (error) {
      this.logAxiosError(error)
      return;
    }
  }
};