const axios = require('axios');

module.exports = class Traefik {
  constructor(apiEndpoint, username, password) {
    this.apiEndpoint = apiEndpoint
    this.username = username
    this.password = password
  }

  getAxiosConfig() {
    return {
      auth: {
        username: this.username,
        password: this.password
      }
    }
  }

  logAxiosError(error) {
    if (error.request) {
      console.error("Request body:", error?.request?.body)
      console.error("HTTP status: ", error?.response?.status)
      console.error("Response data:", error?.response?.data)
    }
    else {
      console.log(error)
    }
  }

  async auth() {
    // we dont actuall auth here, just make sure we can talk to traefik
    try {
      const authResponse = await axios.get(`${this.apiEndpoint}/http/services`, this.getAxiosConfig())

      if (!authResponse.data || !authResponse.data.length) {
        return false;
      }

      return true;
    }
    catch (error) {
      this.logAxiosError(error)
      return false;
    }
  }
}