const commandLineArgs = require('command-line-args');
const Portainer = require('./lib/portainer');
const Traefik = require('./lib/traefik');

const portainerApiEndpoint = 'https://portainer.utilitybelt.me/api'
const traefikApiEndpoint = 'https://traefik.utilitybelt.me/api'
const optionDefinitions = [
  { name: 'tag', alias: 't', type: String },
  { name: 'sha', alias: 's', type: String },
  { name: 'domain', alias: 'd', type: String },
  { name: 'env', alias: 'e', type: String },
]

const options = commandLineArgs(optionDefinitions)

const tag = options['tag']
const sha = options['sha']
const env = options['env']
const domain = options['domain']
const apiServerImage = `registry.gitlab.com/utilitybelt/utilitybelt.me/api-server:${tag}-${sha}`

console.log("Tag:", tag);
console.log("Sha:", sha);
console.log("Env:", env);
console.log("Domain:", domain);
console.log("Api Server Image:", apiServerImage);

function exit(reason) {
  console.error(reason);
  process.exit(1);
}

(async () => {
  const portainer = new Portainer(portainerApiEndpoint, process.env.PORTAINER_USER, process.env.PORTAINER_PASS);
  const traefik = new Traefik(traefikApiEndpoint, process.env.TRAEFIK_USER, process.env.TRAEFIK_PASS)

  if (!await portainer.auth()) {
    return exit("Unable to authenticate with portainer")
  }

  if (!await traefik.auth()) {
    return exit("Unable to authenticate with traefik")
  }

  const stackName = await portainer.getUniqueStackName(`${domain.replace(/\./g, '-')}-${sha}`)

  console.log("Creating new stack:", stackName)
  const stack = portainer.createStack(stackName, getStackFileContents())
  if (!stack) {
    exit("   ... unable to create stack")
  }
  console.log("   ... created");

  await portainer.waitForStackHealth(stackName);
  await portainer.removeOldStacks(domain.replace(/\./g, '-'), stackName)

  console.log(`\n\nNew stack ${stackName} is live at https://${domain}/`)
})()


function getStackFileContents() {
  const serviceName = `${domain.replace(/\./g, '-')}`
  return `services:
  api:
    networks:
      - traefik
    image: ${apiServerImage}
    environment:
      - NODE_ENV=${env}
    labels:
      - traefik.enable=true
      - traefik.http.services.api-server-${serviceName}.loadbalancer.server.port=3000
      - traefik.http.routers.api-server-${serviceName}.rule=Host(\`${domain}\`)
      - traefik.http.routers.api-server-${serviceName}.entrypoints=websecure
      - traefik.http.routers.api-server-${serviceName}.tls.certresolver=letsencrypt
    restart: unless-stopped
networks:
  traefik:
    name: traefik
    external: true
`
}