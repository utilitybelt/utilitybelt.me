# UtilityBelt.me

Source for [utilitybelt.me](https://utilitybelt.me/) website.

## Development

Source is located in the `src/` directory. Make sure you're in there before running the commands below

### Setup

Make sure to install the dependencies:

```bash
# in `src/` directory
yarn install
```

### Development Server

Start the development server on `http://localhost:3000`:

```bash
# in `src/` directory
yarn dev
```

### Production Builds

Build the application for production:

```bash
# in `src/` directory
yarn build
```

Locally preview production build:

```bash
# in `src/` directory
yarn preview
```

## Used Libraries

 - [Nuxt](https://nuxt.com/)
 - [Nuxt Content](https://content.nuxt.com/)
 - [NuxtUI](https://ui.nuxt.com/)
 - [Tailwind CSS](https://tailwindcss.com/)
 - [Iconify](https://iconify.design/)

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.
